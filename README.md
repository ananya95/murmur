# murmur
mumur is a slack clone, composed of multiple things.
`murmur-web` is the django app. See the dedicated README for it.

The project requires [RabbitMQ](https://www.rabbitmq.com) running the
[`web-stomp`](https://www.rabbitmq.com/web-stomp.html) and [HTTP auth
backend](https://github.com/rabbitmq/rabbitmq-auth-backend-http/) plugins.

# Installation
## RabbitMQ
1. Install RabbitMQ.
2. Enable some useful plugins:
   ```shell
   sudo rabbitmq_plugins enable rabbitmq_auth_backend_http 
   sudo rabbitmq_plugins enable rabbitmq_web_stomp rabbitmq_management
   ```
3. Copy (don't symlink) the `rabbitmq.conf` in this folder to [wherever it should
   be](https://www.rabbitmq.com/configure.html#config-location). Usually that's
   `/etc/rabbitmq/`.
4. Start the service using `sudo systemctl start rabbitmq.service`.

## Javascript assets for Django
1. Install [`browserify`](https://browserify.org) using `npm`: `sudo npm install -g browserify`
2. Go to `murmur_js/` and build the javascript required by django:
``` shell
cd murmur_js/
npm install
make
```

## Django and postgres
It is recommended to start a virtualenv using `virtualenv` or `pipenv --python
3.6`. Up to you though.

1. Setup up postgres on your machine. Create a database called `murmur`.
2. Run `pipenv install`
3. Run the migrations (and optionally add a superuser):

``` shell
python murmur_web/manage.py makemigrations core
python murmur_web/manage.py migrate
```
4. Seed some data into the DB. See `core/commands/seed.py` for details.
``` shell
python murmur_web/manage.py seed
```
The admin user is `admin`, `admin`.
5. Launch django-server: `python murmur_web/manage.py runserver`
6. Now that RabbitMQ can authenticate and authorize requests using murmur_web,
   we must configure the default exchanges and queues: `python
   murmur_web/manage.py rabbit_setup`

