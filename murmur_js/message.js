exports.render = function (message) {
    body = $("<div>").addClass("body").text(message.body);
    sender = $("<div>").addClass("sender")
        .attr("name", message.sender.name)
        .attr("user_id", message.sender.id)
        .text(message.sender.name);
    msg_div = $("<div>").attr("id", message.body.id);
    msg_div.append(sender);
    msg_div.append(body);
    console.log(msg_div);
    //return $("<code class='d-block'>").text(message.body);
    return msg_div;
};

exports.render_channel = function (block, channel){
    $(block).empty();
    $.each(window.buffers[channel], function (index, message){
        //console.log(message);
        $(block).append(exports.render(message));
    });
}

/*
    <div id="message-3">
      <div class="sender" name="metallica" user_id="14">
        metallica
      </div>
      <div class="body">
        peace.
      </div>
    </div>

 */

/* leave channel
  switch channel
*/

/* switch_channel
  render buffer  
*/

/* update_view(action) -- only active channel updates should arrive here
* append msg (index of message in buffer)
  - in-thread
  - root-thread
* delete msg (index of message in buffer)
*/

/*
DISPATCH

* append ->
  resize_buffer -- must remove old from html!
  if active channel?
    add_to_view
    remove_from_view
    bump_channel_unread

* delete ->
  resize_buffer
  if in buffer?
    remove_from_view

* send == append

* new-member ->
  update members
  if I'm the new guy!
    update channel list

* kill-member ->
    update members
    if that's me!
      from ws?
        redirect dashboard
      from channel?
        resize_buffer
        update channel list
        switch channel
*/
