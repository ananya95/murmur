exports.channels = JSON.parse(document.getElementById('channels-data').textContent);
exports.workspace = JSON.parse(document.getElementById('workspace-data').textContent);
exports.members = JSON.parse(document.getElementById('members-data').textContent);
exports.messages = JSON.parse(document.getElementById('messages-data').textContent);

channels = JSON.parse(document.getElementById('channel-list-data').textContent);
exports.channel_list = channels.filter(function (v) {return v != null;});

exports.channel_click_listener = function (event){
    $('#channels li').removeClass('active');
    $(event.target).addClass('active');
};

exports.routing_key = function (){
    return $('#channels li.active')[0].attributes.id.value;
};
exports.channel_name = function (){
    return $('#channels li.active')[0].attributes.id.value.split('.')[1];
};

//exports.switch_channel = function
