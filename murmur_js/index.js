var nanoid = require('nanoid');
require('./stomp.js');
var utils = require('./utils.js');
var Message = require('./message.js');

/*************************************************** INITIALIZATION */

$('#channels li').click(utils.channel_click_listener);

var has_had_focus = false;
var sessionid = document.cookie.match('[; ]?sessionid=([^\\s;]*)')[1];
var user_id = document.cookie.match('[; ]?user_id=([^\\s;]*)')[1];
var username = document.cookie.match('[; ]?username=([^\\s;]*)')[1];
window.buffers = utils.messages;
/*************************************************** IMPORTANT FUNCS */

var pipe_messages = function(el_name, send){
    var div = $(el_name + ' #messages');
    var inp  = $(el_name + ' input');
    var form = $(el_name + ' form');

    var print = function(message) {
        dest_channel = message.headers.destination.split('.')[1];
        console.log(dest_channel);
        var update_now = false;
        var mObj = JSON.parse(message.body);
        if (dest_channel[0] == '$'){
            console.log('weird');
            actually_sent_by = mObj.sender.id;
            if (actually_sent_by == user_id){
                console.log('me send');
                //sent_by_me
                window.buffers[dest_channel].push(mObj);
                update_now = dest_channel == utils.channel_name();
            }
            else{
                //recvd_by_me
                if (dest_channel.split('-')[1] == user_id){
                    window.buffers["$user-" + actually_sent_by].push(mObj);
                    update_now = ("$user-" + actually_sent_by) == utils.channel_name();//dest_channel == utils.channel_name();
                }
            }
        }
        else {
            update_now = true;
            console.log('normal');
        }
        if (update_now){
            div.append(Message.render(mObj));
            //div.scrollTop(div.scrollTop() + 10000);
        }
    };
    form.submit(function() {
        send(inp.val());
        inp.val('');
        return false;
    });
    return print;
};

var pipe_logs = function(el_name) {
    var div  = $(el_name + ' div');

    var print = function(message) {
        div.append($("<code>").text(message));
        div.scrollTop(div.scrollTop() + 10000);
    };
    return print;
};

/*************************************************** STOMP JS */
var client = Stomp.client("ws://localhost:15674/ws");
client.debug = pipe_logs('#second');

var real_time_dump = pipe_messages('#channel-view', function(data) {
    now = new Date();
    message = {
        'id': nanoid(),
        'type': 'text',
        'body': data,
        'timestamp': now.toISOString(),
        'sender': {'id': user_id, 'name': username}
    }
    client.send('/exchange/mr/'+ utils.routing_key(), {
        "content-type":"application/json",
        "sessionid": sessionid
    }, JSON.stringify(message));
});

var on_connect = function(x) {
    for (var channel of utils.channel_list){
        client.subscribe("/exchange/"+utils.workspace+"/"+channel.id, function(d) {
            real_time_dump(d);
        });
    }
};
var on_error =  function() {
    console.log('error');
};

$('#channel-view input').focus(function() {
    if (!has_had_focus) {
        has_had_focus = true;
        $(this).val("");
    }
});

/********************************************* MAIN MAIN */
client.connect(sessionid, 'notapassword', on_connect, on_error, '/');
//console.log(utils.messages);
Message.render_channel('#messages', utils.channel_name());
$('#channels li').click(function (event){
    Message.render_channel('#messages', utils.channel_name());
});

