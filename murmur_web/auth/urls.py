from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('user/',     views.user, name='user'),
    path('vhost/',    views.vhost, name='vhost'),
    path('resource/', views.resource, name='resource'),
    path('topic/',    views.topic, name='topic')
]
