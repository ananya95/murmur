from django.http import HttpResponse
from django.contrib.sessions.models import Session
from core.models import *
import core.rabbitmq
import re

import logging
logger = logging.getLogger(__name__)

SPECIAL_CHANNEL_RE = re.compile(r'([^\.]+)\.\$user-(.*)')
SPECIAL_USERS = {
    'admin': 'admin'
}

def catch_special(operation):
    def simple_operation(request, *args, **kwargs):
        if 'username' in request.GET:
            username = request.GET['username']
            if username in SPECIAL_USERS:
                return HttpResponse("allow")
            else:
                s = Session.objects.get(pk=username).get_decoded()
                user = Person.objects.get(pk=s['_auth_user_id'])
                return operation(request, user)
        else:
            return HttpResponse("deny")
    return simple_operation

def user(request):
    if 'username' in request.GET and 'password' in request.GET:
        username = request.GET['username']
        if username in SPECIAL_USERS and SPECIAL_USERS[username] == request.GET['password']:
            return HttpResponse("allow administrator")
        else:
            s = Session.objects.get(pk=username).get_decoded()
            user = Person.objects.get(pk=s['_auth_user_id'])
            if user:
                return HttpResponse("allow management")
    return HttpResponse("deny")

# Ignore this, not important
@catch_special
def vhost(request, user):
    if 'vhost' in request.GET:
        if request.GET['vhost'] == '/' and user:
            return HttpResponse("allow")
    return HttpResponse("deny")

@catch_special
def resource(request, user):
    if user:
        thing = request.GET['resource']
        name = request.GET['name']
        perm = request.GET['permission']
        return check_resource_permission(user, thing, name, perm)
    else:
        return HttpResponse("deny")

@catch_special
def topic(request, user):
    if user:
        rkey = request.GET['routing_key']
        name = request.GET['name']
        perm = request.GET['permission']
        return check_topic_permission(user, rkey, name, perm)
    return HttpResponse("deny")

# Helpers
def check_resource_permission(user, thing, name, perm):
    if thing == 'queue':
        return HttpResponse("allow")
    if thing == 'exchange':
        if name == core.rabbitmq.MURMUR_ROOT_EXCHANGE:
            if perm == 'write':
                return HttpResponse("allow")
        elif (user.workspace_set.filter(name=name).count() == 1 and
              perm in ['read', 'write']):
            return HttpResponse("allow")

def check_topic_permission(user, rkey, ws_name, perm):
    if perm == 'write' and ws_name == core.rabbitmq.MURMUR_ROOT_EXCHANGE:
        potential_match = SPECIAL_CHANNEL_RE.match(rkey)
        if potential_match:
            # destination is a user in the same workspace
            # need to do more work!
            real_ws_name = potential_match.group(1)
            if (user.workspace_set.filter(name=real_ws_name).count() == 1 and
                Person.objects.filter(workspacemembership__workspace__name=real_ws_name,
                                      pk=potential_match.group(2)).count() == 1):
                return HttpResponse("allow")
        else:
            real_ws_name, ch_name = rkey.split('.')
            if user.channel_set.filter(name=ch_name, workspace__name=real_ws_name).count() == 1:
            # user is publishing to a valid channel of the workspace.
                return HttpResponse("allow")
    elif perm in ['read'] and ws_name != core.rabbitmq.MURMUR_ROOT_EXCHANGE:
        # destination is not the root exchange
        potential_match = SPECIAL_CHANNEL_RE.match(rkey)
        if potential_match:
            # user is subscribing to a user in the same workspace
            # need to do more work
            if (user.workspace_set.filter(name=ws_name).count() == 1 and
                Person.objects.filter(workspacemembership__workspace__name=ws_name,
                                      pk=potential_match.group(2)).count() == 1):
                return HttpResponse("allow")
        else:
            # user is subscribing to a valid channel of the workspace.
            real_ws_name, ch_name = rkey.split('.')
            if user.channel_set.filter(name=ch_name, workspace__name=real_ws_name).count() == 1:
                return HttpResponse("allow")
    return HttpResponse("deny")
