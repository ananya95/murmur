from django.db import models
from django.contrib.auth.models import AbstractUser
import core.rabbitmq

# Create your models here.
class Person(AbstractUser):
    email_verified = models.BooleanField(default=False)

    class Meta:
        unique_together = ('email',)


class RegistrationToken(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    token = models.CharField(max_length=32)
    is_verified = models.BooleanField(default=False)


class Channel(models.Model):
    name = models.CharField(max_length=120)
    members = models.ManyToManyField(Person, through='ChannelMembership', through_fields=('channel', 'person'))
    workspace = models.ForeignKey('Workspace', on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def get_messages(self, limit=20):
        messages = self.message_set.all().order_by('timestamp')[:limit]
        return [x.to_json() for x in messages]

    def __str__(self):
        return ("%s" % self.name)

    def create(ch_name, ws, user, initial_members=[]):
        ch = __class__(name=ch_name, workspace=ws)
        ch.save()
        for member in initial_members:
            ChannelMembership.objects.create(channel=ch, person=member, invited_by=user)
        ch.save()
        return ch


class ChannelMembership(models.Model):
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    invited_by = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='invited_channel_members')
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('person', 'channel', 'invited_by')


class Workspace(models.Model):
    name = models.CharField(max_length=120, unique=True)
    members = models.ManyToManyField(Person, through='WorkspaceMembership', through_fields=('workspace', 'person'))
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('name',)

    def __str__(self):
        return ("%s" % self.name)

    def admin(self):
        return self.members.filter(workspacemembership__is_admin=True)
    
    def create_and_declare(name, admin, initial_members=[]):
        ws = __class__(name=name)
        ws.save()
        WorkspaceMembership.objects.create(workspace=ws,
                                           person=admin,
                                           invited_by=admin,
                                           is_admin=True)
        with core.rabbitmq.open() as ri:
            ri.declare_workspace(name, exchange_type='topic', durable=True)
        return ws


    def delete_and_purge(self):
        self.delete()
        with core.rabbitmq.open() as ri:
            ri.delete_workspace(self.name)

class WorkspaceMembership(models.Model):
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    nick = models.CharField(max_length=120, blank=False)
    invited_by = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='invited_ws_members')
    is_admin = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('person', 'nick', 'workspace', 'invited_by', 'is_admin')

    def get_messages(self, reciever, limit=20):
        if reciever == self.person:
            sent_by_me = list(Message.objects.filter(workspace=self.workspace, reciever=reciever, sender=self.person))
            return [x.to_json() for x in sent_by_me]
        else:
            sent_by_me = Message.objects.filter(workspace=self.workspace, reciever=reciever, sender=self.person)
            recvd_by_me = Message.objects.filter(workspace=self.workspace, reciever=self.person, sender=reciever)
            return [x.to_json() for x in sent_by_me.union(recvd_by_me).order_by('timestamp')]

class Message(models.Model):
    message_id = models.CharField(max_length=25, primary_key=True)
    body = models.TextField()
    mtype = models.CharField(max_length=12)
    timestamp = models.DateTimeField(auto_now_add=True)
    sender = models.ForeignKey(Person, null=True, on_delete=models.SET_NULL, related_name='as_sender')
    reciever = models.ForeignKey(Person, null=True, on_delete=models.SET_NULL, related_name='as_reciever')
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE, null=True)
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)
    parent = models.ForeignKey('Message', on_delete=models.CASCADE, null=True)

    class Meta:
        unique_together = ('message_id', 'sender', 'parent', 'workspace')

    def to_json(self):
        sender_nick_name = WorkspaceMembership.objects.get(workspace=self.workspace, person=self.sender).nick
        # reciever = None
        # if self.reciever:
        #     reciever_nick_name = WorkspaceMembership.objects.get(workspace=self.workspace, person=self.reciever).nick
        #     reciever = {'id': self.reciever.id,
        #                 'name': reciever_nick_name}
        return {'id': self.message_id,
                'body': self.body,
                'type': self.mtype,
                'timestamp': self.timestamp.isoformat(),
                'sender': {'id': self.sender.id,
                           'name': sender_nick_name},
                'parent': self.parent.id if self.parent else None,}
                # 'reciever': reciever}
