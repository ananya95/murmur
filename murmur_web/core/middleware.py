class UserCookieMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if (request.user.is_authenticated and
            (request.COOKIES.get('username') is None or request.COOKIES.get('user_id') is None)):
            response.set_cookie('username', request.user.username)
            response.set_cookie('user_id', request.user.id)
        elif not request.user.is_authenticated:
            response.delete_cookie('username')
            response.delete_cookie('user_id')
        return response
