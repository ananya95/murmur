import pika
from django.core.management.base import BaseCommand, CommandError
import core.rabbitmq

class Command(BaseCommand):
    help = 'Initialize rabbitmq for murmur'

    def add_arguments(self, parser):
        #parser.add_argument('poll_id', nargs='+', type=int)
        pass

    def handle(self, *args, **options):
        with core.rabbitmq.open() as ri:
            ri.channel.exchange_declare(core.rabbitmq.MURMUR_ROOT_EXCHANGE, exchange_type='topic', durable=True)
            ri.channel.queue_declare(core.rabbitmq.MURMUR_DB_QUEUE, durable=True)
            ri.channel.queue_bind(core.rabbitmq.MURMUR_DB_QUEUE, core.rabbitmq.MURMUR_ROOT_EXCHANGE, '*.*')
        self.stdout.write(self.style.SUCCESS('RabbitMQ configured. Check `sudo rabbitmqctl list_bindings`'))
