import django.db.utils
from django.core.management.base import BaseCommand, CommandError
from core.models import *
import core.rabbitmq as rmq

class Command(BaseCommand):
    help = 'Seed some stuff'

    def add_arguments(self, parser):
        #parser.add_argument('poll_id', nargs='+', type=int)
        pass
    
    def handle(self, *args, **options):
        #raise CommandError('Poll "%s" does not exist' % poll_id)
        try:
            Person.objects.create_user(username='admin', email='admin@murmur.com', password='admin')
            Person.objects.create_user(username='metallica', email='metallica@murmur.com', password='murmur')
            Person.objects.create_user(username='disturbed', email='disturbed@murmur.com', password='murmur')
            Person.objects.create_user(username='megadeath', email='megadeath@murmur.com', password='murmur')
        except django.db.utils.IntegrityError:
            pass
        admin, metallica, disturbed, megadeath = [Person.objects.get(username=name) for name in ['admin', 'metallica', 'disturbed', 'megadeath']]
        persons = [admin, metallica, disturbed, megadeath]
        self.stdout.write(self.style.SUCCESS('Added %d persons' % len(persons)))
        
        (test_ws, test_members) = self.make_workspace('__test', persons, admin)
        (utopia_ws, utopia_members) = self.make_workspace('utopia', persons[1:], metallica)
        wss = [test_ws, utopia_ws]

        self.make_channel(utopia_ws, 'general', persons[1:])
        self.make_channel(utopia_ws, 'random', persons[2:])
        self.make_channel(utopia_ws, 'lonely', persons[3:])
        self.make_channel(test_ws, 'general', persons)
        self.make_channel(test_ws, 'random', persons)
        self.make_channel(test_ws, 'bakchodi', persons)

    def make_workspace(self, name, members, admin):
        (ws, _) = Workspace.objects.get_or_create(name=name)
        ws_members = [WorkspaceMembership.objects.get_or_create(workspace=ws,
                                                                person=p,
                                                                nick=p.username+'_nick',
                                                                is_admin=(p == admin),
                                                                invited_by=admin)
                      for p in members]
        self.stdout.write(self.style.SUCCESS('Added %d members to ws %s' % (len(ws_members), name)))
        with rmq.open() as ri:
            ri.declare_workspace(name, exchange_type='topic', durable=True)
        return ws, [wm for (wm, _) in ws_members]
        
    def make_channel(self, ws, name, members):
        (c, _) = Channel.objects.get_or_create(name=name, workspace=ws)
        c_members = [ChannelMembership.objects.get_or_create(channel=c,
                                                             person=m,
                                                             invited_by=members[0])
                     for m in members]
        self.stdout.write(self.style.SUCCESS('Added %d members to %s.%s' % (len(c_members), ws.name, name)))
        return c, [cm for (cm, _) in c_members]
