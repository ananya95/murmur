import pika
import datetime
import json
from django.core.management.base import BaseCommand, CommandError
import core.rabbitmq
from core.models import *
from django.contrib.sessions.models import Session
from django.utils import timezone

class Command(BaseCommand):
    help = 'Initialize rabbitmq for murmur'

    def add_arguments(self, parser):
        #parser.add_argument('poll_id', nargs='+', type=int)
        pass

    def callback(self, ch, method, properties, body):
        self.stdout.write(self.style.SUCCESS(body))
        dbody = json.loads(body)
        # {"id":"8rCB7b9ulffhF6MOdk2_f","type":"text","body":"djbh98re8j","sender":{"id":-1,"name":"__void__"}, "parentid" : "SOMETHING"} 
        dhead = properties
        # {'content-length': '99', 'sessionid': '9wxqa4miq646vc8nbbpvnffmgg3znmb5'
        self.stdout.write(self.style.SUCCESS(dhead))
        rk = method.routing_key
        ws, chan = rk.split('.')
        s = Session.objects.get(pk=properties.headers['sessionid']).get_decoded()
        user = Person.objects.get(pk=s['_auth_user_id'])
        work_obj = Workspace.objects.get(name = ws)
        if chan.split('-')[0] == '$user':
            pm_target = Person.objects.get(pk=int(chan.split('-')[1]))
            m = Message(message_id=dbody['id'],
                        channel = None,
                        sender = user,
                        reciever = pm_target,
                        body = dbody['body'],
                        parent = None,
                        mtype = dbody['type'],
                        timestamp = timezone.now(),
                        workspace=work_obj)
        else:
            channel_obj = Channel.objects.get(name=chan,workspace = work_obj.id )
            m = Message(message_id=dbody['id'],
                        channel = channel_obj,
                        sender = user,
                        reciever = None,
                        body = dbody['body'],
                        parent = None,
                        mtype = dbody['type'],
                        workspace=work_obj)

        m.save()


    def handle(self, *args, **options):
        with core.rabbitmq.open() as ri:
            ri.channel.basic_consume(self.callback, queue=core.rabbitmq.MURMUR_DB_QUEUE, no_ack=True)
            ri.channel.start_consuming()
            self.stdout.write(self.style.SUCCESS('RabbitMQ configured. Check `sudo rabbitmqctl list_bindings`'))
