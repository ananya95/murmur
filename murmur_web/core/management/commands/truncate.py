from django.core.management.base import BaseCommand, CommandError
from core.models import *
        
class Command(BaseCommand):
    help = 'Truncate all tables!'

    def add_arguments(self, parser):
        parser.add_argument('-m', action='store_true', default=False, dest='messages')
    
    def handle(self, *args, **options):
        #raise CommandError('Poll "%s" does not exist' % poll_id)
        from django.db import connection
        cursor = connection.cursor()
        if options['messages']:
            cursor.execute("truncate table %s" % Message._meta.db_table)
            self.stdout.write(self.style.SUCCESS('Truncated Messages.'))
        else:
            cursor.execute("truncate table %s cascade" % Person._meta.db_table)
            self.stdout.write(self.style.SUCCESS('Truncated Person etc.'))

            cursor.execute("truncate table %s cascade" % Workspace._meta.db_table)
            self.stdout.write(self.style.SUCCESS('Truncated Workspace etc.'))

            cursor.execute("truncate table %s cascade" % Channel._meta.db_table)
            self.stdout.write(self.style.SUCCESS('Truncated Channel etc.'))
            
            cursor.execute("truncate table %s cascade" % RegistrationToken._meta.db_table)
            self.stdout.write(self.style.SUCCESS('Truncated RegistrationToken etc.'))
