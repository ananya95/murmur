import django.db.utils
from django.core.management.base import BaseCommand, CommandError
from core.models import *
import core.rabbitmq as rmq

class Command(BaseCommand):
    help = 'Seed some stuff'

    def add_arguments(self, parser):
        #parser.add_argument('poll_id', nargs='+', type=int)
        pass
    
    def handle(self, *args, **options):
        #raise CommandError('Poll "%s" does not exist' % poll_id)
        try:
            Person.objects.create_user(username='harvey', email='ananya@cse.iitb.ac.in', password='murmur')
            Person.objects.create_user(username='mike', email='mike@murmur.com', password='murmur')
            Person.objects.create_user(username='rachel', email='rachel@murmur.com', password='murmur')
            Person.objects.create_user(username='louis', email='louis@murmur.com', password='murmur')
        except django.db.utils.IntegrityError:
            pass
        harvey, mike, rachel, louis = [Person.objects.get(username=name) for name in ['harvey', 'mike', 'rachel', 'louis']]
        persons = [harvey, mike, rachel, louis]
        self.stdout.write(self.style.SUCCESS('Added %d persons' % len(persons)))
        
        #(test_ws, test_members) = self.make_workspace('__test', persons, admin)
        (suits_ws, suits_members) = self.make_workspace('suits', persons, harvey)
        wss = [suits_ws]

        self.make_channel(suits_ws, 'general', persons)
        self.make_channel(suits_ws, 'admin-stuff', [harvey, louis])
        self.make_channel(suits_ws, 'lonely', [mike, rachel])

    def make_workspace(self, name, members, admin):
        (ws, _) = Workspace.objects.get_or_create(name=name)
        ws_members = [WorkspaceMembership.objects.get_or_create(workspace=ws,
                                                                person=p,
                                                                nick=p.username,
                                                                is_admin=(p == admin),
                                                                invited_by=admin)
                      for p in members]
        self.stdout.write(self.style.SUCCESS('Added %d members to ws %s' % (len(ws_members), name)))
        with rmq.open() as ri:
            ri.declare_workspace(name, exchange_type='topic', durable=True)
        return ws, [wm for (wm, _) in ws_members]
        
    def make_channel(self, ws, name, members):
        (c, _) = Channel.objects.get_or_create(name=name, workspace=ws)
        c_members = [ChannelMembership.objects.get_or_create(channel=c,
                                                             person=m,
                                                             invited_by=members[0])
                     for m in members]
        self.stdout.write(self.style.SUCCESS('Added %d members to %s.%s' % (len(c_members), ws.name, name)))
        return c, [cm for (cm, _) in c_members]
