from django.contrib import admin
from .models import Channel, Workspace, Person

# Register your models here.
admin.site.register(Channel)
admin.site.register(Workspace)
admin.site.register(Person)
