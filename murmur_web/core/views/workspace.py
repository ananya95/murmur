from django.shortcuts import render, redirect
from django.http import HttpResponse
from core.models import Workspace, Channel, WorkspaceMembership, ChannelMembership
from django.contrib.auth.decorators import login_required
# from core.permisions import permission_required
from django import forms
from django.apps import apps
import logging

logger = logging.getLogger(__name__)


class ws_form(forms.Form):
    ws_name = forms.CharField(label='New workspace name')
    ch_name = forms.CharField(label='Channel name', help_text="One is compulsory")

    class Meta:
        model = Workspace
        model = Channel


class ch_form(forms.Form):
    ch_name = forms.CharField(label='New channel name')

    class Meta:
        model = Channel


# Create your views here
'''
Any user can create a workspace and become it's admin.
In the form shown here:
* Get workspace name etc.
* Get Atleast 1 channel name.
* Get list of emails IDs (for invitations)
'''


@login_required
def create(request):
    if request.method == 'POST':
        form = ws_form(request.POST)
        if form.is_valid():
            ws_name = form.cleaned_data.get('ws_name')
            ch_name = form.cleaned_data.get('ch_name')
            admin = request.user
            ws = Workspace.create_and_declare(name=ws_name, admin=admin)
            Channel.create(ch_name=ch_name, ws=ws, user=admin, initial_members=[admin])
            return redirect('/')
    else:
        form = ws_form()
    return render(request, 'core/workspace/ws_create.html', {'form': form})


'''
Admins see all channels, others don't
'''


@login_required
def home(request, ws_name):
    if request.user.workspace_set.filter(name=ws_name).count() == 1:
        channels = request.user.channel_set.filter(workspace__name=ws_name).order_by('created_on')
        members = WorkspaceMembership.objects.filter(workspace__name=ws_name).order_by('created_on')
        messages = {channel.name: channel.get_messages(30) for channel in channels}
        private_stuff = {"$user-{}".format(member.person.id): member.get_messages(request.user, 30) for member in members}
        messages.update(private_stuff)
        utils = {
            'channels': [{'name': c.name, 'id': '{}.{}'.format(ws_name, c.name)} for c in channels] + [None] + [{'name': m.nick, 'id': '{}.$user-{}'.format(ws_name, m.person.id)} for m in members]}

        return render(request, 'core/workspace/home.html', {
            'channels': [x.name for x in channels],
            'members': [{'id': x.person.id,
                         'name': x.nick} for x in members],
            'workspace': ws_name,
            'utils': utils,
            'messages': messages,
            'is_admin': request.user.workspacemembership_set.filter(workspace__name=ws_name, is_admin=True).count() == 1})
    else:
        redirect('login')

'''
An admin should be able to
* Send invitation emails.
* Add/remove members of channels from the existing ws members.
* Remove members from ws (persons retain ws membership even when removed from all channels).
  - Removal from ws, leads to removal from all channels.
* Grant/revoke `workspace_admin` privilege to other members.
* Delete the workspace.
'''


@login_required
# @permission_required('core.workspace_admin')
def settings(request, ws_name):
    if request.user.workspace_set.filter(name=ws_name).count() == 1:
        # isadmin = request.user.workspacemembership_set.filter(workspace__name=ws_name, is_admin=True).count() == 1

        payload = {}
        channels = request.user.channel_set.filter(workspace__name=ws_name)
        for ch in channels:
            name = list()
            for mem in ChannelMembership.objects.filter(channel=ch).order_by('created_on'):
                name.append(mem.person.username)
            payload[ch.name] = name

        return render(request, 'core/workspace/ws_setting.html', {
        'data': payload, 'workspace': ws_name
        })

    # return render(request, 'core/workspace/ws_setting.html', {'ws_name': ws_name})

'''
In the form shown here:
* Get name, etc.
* List of workspace members to add in here.

Channel names cannot have `$` as the first character!
'''

@login_required
# @permission_required('core.workspace_admin')
def new_channel(request, ws_name):
    if request.method == 'POST':
        form = ch_form(request.POST)
        if form.is_valid():
            ch_name = form.cleaned_data.get('ch_name')
            admin = request.user
            ws = Workspace.objects.get(name=ws_name)
            Channel.create(ch_name=ch_name, ws=ws, user=admin, initial_members=[admin])
            return redirect('/')
    else:
        form = ch_form()
    return render(request, 'core/workspace/ws_newch.html', {'form': form, 'ws_name': ws_name})
