from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from . import user as user_views
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model

import logging

logger = logging.getLogger(__name__)

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        User = get_user_model()
        model = User
        fields = ('username', 'email', 'password1', 'password2')


# Create your views here.
def home(request):
    if request.user.is_authenticated:
        return user_views.dashboard(request)
    else:
        return redirect('login')


def workspace(request):
    pass


def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('user:dash')
    else:
        form = SignUpForm()
    return render(request, 'core/registration/register.html', {'form': form})
