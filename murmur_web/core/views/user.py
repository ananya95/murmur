from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

import logging
logger = logging.getLogger(__name__)

# Create your views here.
@login_required
def dashboard(request):
    ws_list = [x.name for x in request.user.workspace_set.all()]
    return render(request, 'core/user/dashboard.html', {'ws_list': ws_list})


@login_required
def settings(request):
    return HttpResponse("wow, %s/settings" % request.user.username)

@login_required
def sudo(request):
    ws_name = 'utopia'
    channels = [x.name for x in request.user.channel_set.filter(workspace__name=ws_name).order_by('created_on')]
    members = [{'id': x.person.id,
                'name': x.nick}
               for x in WorkspaceMembership.objects
                   .filter(workspace__name=ws_name)
                   .order_by('created_on')]

    utils = {
        'channels': [{'name': c, 'id': '{}.{}'.format(ws_name, c)} for c in channels] + [None] + [{'name': m['name'], 'id': '{}.$user-{}'.format(ws_name, m['id'])} for m in members]}
    return render(request, 'core/user/sudo.html', {
        'channels': channels,
        'members': members,
        'workspace': ws_name,
        'utils': utils})
