import pika

MURMUR_ROOT_EXCHANGE = 'mr'
MURMUR_DB_QUEUE = 'mrq-db'
ADMIN = 'admin'
ADMIN_PASSWORD = 'admin'

def open(username=ADMIN, password=ADMIN_PASSWORD):
    return RabbitInterface(username, password)

class RabbitInterface:
    def __init__(self, username, password):
        credentials = pika.PlainCredentials(username, password)
        self.params = pika.ConnectionParameters('localhost', credentials=credentials)
    
    def declare_workspace(self, name, *args, **kwargs):
        assert kwargs['durable']
        self.channel.exchange_declare(name, *args, **kwargs)
        return self.channel.exchange_bind(name, MURMUR_ROOT_EXCHANGE, routing_key='%s.*' % name)
    
    def delete_workspace(self, name):
        return self.channel.exchange_delete(name)

    def __enter__(self):
        self.connection = pika.BlockingConnection(self.params)
        self.channel = self.connection.channel()
        if self.channel:
            return self
        else:
            raise Exception('Not connected to RMQ. Use a `with` context.')

    def __exit__(self, type, value, traceback):
        if self.connection and self.channel:
            self.channel.close()
            self.connection.close()
