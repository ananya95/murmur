from django.urls import path, re_path, include
import django.contrib.auth.views as auth_views
from . import views

workspace_patterns = ([
    path('new/', views.workspace.create, name='create'),
    path('<str:ws_name>/', views.workspace.home, name='home'),
    path('<str:ws_name>/new', views.workspace.new_channel, name='new_channel'),
    path('<str:ws_name>/settings', views.workspace.settings, name='settings'),
], 'ws')

user_patterns = ([
    path('dashboard/', views.user.dashboard, name='dash'),
    path('settings/', views.user.settings, name='settings'),
    path('sudo/', views.user.sudo, name='sudo'),
], 'user')

urlpatterns = [
    path('', views.public.home, name='home'),
    path('login/', auth_views.LoginView.as_view(template_name='core/registration/login.html'), name='login'),
    path('register/', views.public.register, name='register'),
    path('', include('django.contrib.auth.urls')),
    path('u/', include(user_patterns, namespace='user')),
    path('w/', include(workspace_patterns, namespace='ws')),
]
