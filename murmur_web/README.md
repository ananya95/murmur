# URL namespacing

Remember that the most important entities are workspaces.
> To see how the URLs are arranged, do `python manage.py show_urls`

| URL                        | View                                                  | name                           |
|----------------------------|-------------------------------------------------------|--------------------------------|
| `/`                        | `core.views.public.cover`                             | public:cover                   |
| `/login/`                  | `django.contrib.auth.views.LoginView`                 | public:login                   |
| `/logout/`                 | `django.contrib.auth.views.LogoutView`                | public:logout                  |
| `/password_change/`        | `django.contrib.auth.views.PasswordChangeView`        | public:password_change         |
| `/password_change/done/`   | `django.contrib.auth.views.PasswordChangeDoneView`    | public:password_change_done    |
| `/password_reset/`         | `django.contrib.auth.views.PasswordResetView`         | public:password_reset          |
| `/password_reset/done/`    | `django.contrib.auth.views.PasswordResetDoneView`     | public:password_reset_done     |
| `/reset/<uidb64>/<token>/` | `django.contrib.auth.views.PasswordResetConfirmView`  | public:password_reset_confirm  |
| `/reset/done/`             | `django.contrib.auth.views.PasswordResetCompleteView` | public:password_reset_complete |
| `/u/dashboard/`            | `core.views.user.dashboard`                           | user:dash                      |
| `/u/settings/`             | `core.views.user.settings`                            | user:settings                  |

1. `public` URLS do not require login.
2. The cover page acts differently when user is loggen in and otherwise. When
   the user is logged in, the dashboard is rendered (but the URL on the browser
   remains `/`).
3. `u/*` URLs are for profile settings etc.

# What (and probably why)
If looking for a library, use this [awesome
list](https://github.com/rosarior/awesome-django).

## Login Signup and emails
Django's utilities are easy to use. Don't need anything fancy at all.

## Roles and Permissions
[django-rules](https://github.com/dfunckt/django-rules): An in-memory decision
tree. No dirty DB crap.
